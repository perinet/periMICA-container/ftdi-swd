# build container
FROM --platform=linux/arm/v7 docker.io/library/golang:1.21.3-bullseye AS builder
RUN apt-get update
RUN apt-get install -yq git
RUN git clone https://gitlab.com/perinet/periMICA-container/service/flash_stm32g0x.git /usr/local/go/src/base
WORKDIR /usr/local/go/src/base
RUN go build -o output/perimica_webserver


FROM --platform=linux/arm/v7 debian:bullseye-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN uname -m
RUN if [ $(dpkg --print-architecture) != "armhf" ]; then echo "wrong container platform, please check buildah version"; exit 1; fi

LABEL io.perinet.image.is-system-container="true"
LABEL io.perinet.image.init="/sbin/init"

COPY rootfs/ /

COPY --from=builder /usr/local/go/src/base/output/perimica_webserver /usr/bin/

RUN apt-get update
RUN apt-get install --no-install-recommends -y systemd-sysv avahi-daemon avahi-utils libnss-mdns openssl openocd

RUN apt-get autoremove -y
RUN apt-get clean -y

RUN chmod +x /usr/sbin/cert-init.sh

RUN sed -i 's/mdns4_minimal/mdns6_minimal/' /etc/nsswitch.conf

RUN systemctl enable cert-init.service
RUN systemctl enable perimica_webserver.service

CMD [ "sleep", "infinity" ]

